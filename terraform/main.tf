provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#######################
# VPC resources
#######################
resource "aws_vpc" "lutfi" {
  cidr_block           = "${var.cidr_block}"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.name}-vpc"
  }
}

resource "aws_internet_gateway" "lutfi" {
  vpc_id = aws_vpc.lutfi.id
  tags = {
    Name = "${var.name}-ig"
  }
}

resource "aws_route_table" "private_lutfi" {
  count = length(var.private_subnet_cidr_blocks)

  vpc_id = aws_vpc.lutfi.id
  tags = {
    Name = "${var.name}-rt-private"
  }
}

resource "aws_route" "private_lutfi" {
  count = length(var.private_subnet_cidr_blocks)

  route_table_id         = aws_route_table.private_lutfi[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.lutfi[count.index].id
}

resource "aws_route_table" "public_lutfi" {
  vpc_id = aws_vpc.lutfi.id
  tags = {
    Name = "${var.name}-rt-public"
  }
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public_lutfi.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.lutfi.id
}

resource "aws_subnet" "private_lutfi" {
  count = length(var.private_subnet_cidr_blocks)

  vpc_id            = aws_vpc.lutfi.id
  cidr_block        = var.private_subnet_cidr_blocks[count.index]
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "${var.name}-sb-private"
  }
}

resource "aws_subnet" "public_lutfi" {
  count = length(var.public_subnet_cidr_blocks)

  vpc_id                  = aws_vpc.lutfi.id
  cidr_block              = var.public_subnet_cidr_blocks[count.index]
  availability_zone       = var.availability_zones[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.name}-sb-public"
  }
}

resource "aws_route_table_association" "private_lutfi" {
  count = length(var.private_subnet_cidr_blocks)

  subnet_id      = aws_subnet.private_lutfi[count.index].id
  route_table_id = aws_route_table.private_lutfi[count.index].id
}

resource "aws_route_table_association" "public_lutfi" {
  count = length(var.public_subnet_cidr_blocks)

  subnet_id      = aws_subnet.public_lutfi[count.index].id
  route_table_id = aws_route_table.public_lutfi.id
}

#######################
# NAT resources
#######################
resource "aws_eip" "lutfi" {
  count = length(var.private_subnet_cidr_blocks)
  vpc   = true
  tags = {
    Name = "${var.name}"
  }
}

resource "aws_nat_gateway" "lutfi" {
  depends_on    = [aws_internet_gateway.lutfi]
  count         = length(var.private_subnet_cidr_blocks)
  allocation_id = aws_eip.lutfi[count.index].id
  subnet_id     = aws_subnet.private_lutfi[count.index].id
  tags = {
    Name = "${var.name}-ng-private"
  }
}


#######################
# Security group
#######################
resource "aws_security_group" "lutfi" {
  name        = "allow_tls"
  vpc_id      = aws_vpc.lutfi.id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.private_subnet_cidr_blocks[0]}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.name}-sg"
  }
}

#######################
# Launch configuration
#######################
resource "aws_launch_configuration" "lutfi" {
  count                       = var.create_lc ? 1 : 0
  name_prefix                 = "${var.name}-lg-"
  image_id                    = "${var.image_id}"
  instance_type               = "t2.medium"
  key_name                    = "${var.key_name}"
  security_groups             = [aws_security_group.lutfi.id]
  associate_public_ip_address = false
  user_data                   = "${var.user_data}"
  enable_monitoring           = true
  ebs_optimized               = false
  root_block_device {
    volume_size = "20"
  }
}

####################
# Autoscaling group
####################
resource "aws_autoscaling_group" "lutfi" {
  name_prefix = "${var.name}-asg-"
  launch_configuration      = var.create_lc ? element(concat(aws_launch_configuration.lutfi.*.name, [""]), 0) : var.launch_configuration
  vpc_zone_identifier       = [aws_subnet.private_lutfi[0].id]
  max_size                  = 5
  min_size                  = 2
  desired_capacity          = 2
  default_cooldown          = "300"
  health_check_type         = "EC2"
  health_check_grace_period = 300
  force_delete              = true
  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "lutfi" {
  name                      = "${aws_autoscaling_group.lutfi.name}-asg-policy"
  policy_type               = "TargetTrackingScaling"
  estimated_instance_warmup = 200
  autoscaling_group_name    = aws_autoscaling_group.lutfi.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 45
  }
}