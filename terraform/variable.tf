variable "aws_access_key" {
  default     = "XXXXXXXXXX"
  type        = string
}
variable "aws_secret_key" {
  default     = "XXXXXXXX"
  type        = string
}
variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
}

variable "name" {
  default     = "lutfi"
  type        = string
}
variable "cidr_block" {
  default     = "10.0.0.0/16"
  type        = string
}
variable "private_subnet_cidr_blocks" {
  default     = ["10.0.1.0/24"]
  type        = list
}
variable "public_subnet_cidr_blocks" {
  default     = ["10.0.0.0/24"]
  type        = list
}
variable "availability_zones" {
  default     = ["ap-southeast-1a"]
  type        = list
}

variable "create_lc" {
  default     = true
  type        = bool
}
variable "image_id" {
  default     = "ami-8fcc75ec"
  type        = string
}
variable "key_name" {
  default     = "xxxxxxxx"
  type        = string
}
variable "user_data" {
  default     = "#!/bin/bash"
  type        = string
}
variable "launch_configuration" {
  default     = ""
  type        = string
}